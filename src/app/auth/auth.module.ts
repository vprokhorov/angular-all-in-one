import { SignInComponent } from './sign-in/sing-in.component';
import { AuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [SignInComponent],
    imports: [
      AuthRoutingModule,
      FormsModule,
      ReactiveFormsModule,
    ]
})
export class AuthModule {

}
