import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {

  /**
   * Sign in form
   */
  loginForm: FormGroup;

  /**
   * Sign up form
   */
  registerForm: FormGroup;

  /**
   * Check if logged
   */
  loginActive: boolean;

  /**
   * @ignore
   */
  constructor() {
    this.loginActive = true;
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
    });

    this.registerForm = new FormGroup({
      name: new FormControl(''),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      privacy: new FormControl(false),
    });
  }

  /**
   * Call on user sign in submit
   */
  signInSubmit(): void {
    // TODO: sign in logic
    this.loginActive = true;
  }

  /**
   * Call on user sign up submit
   */
  signUpSubmit(): void {
    // TODO: sign up logic
    this.loginActive = false;
  }

}
